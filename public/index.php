<?php

require "../vendor/autoload.php";

use App\TemplateName;
use App\Sidebar;

$exercise = isset($_GET["exercise"]) ? $_GET["exercise"] : "";

$twig = new Twig_Environment(new Twig_Loader_Filesystem("/"), []);

echo $twig->render(TemplateName::get($exercise), [
	"navigation" => Sidebar::get(),
	"exercise" => $exercise,
]);
