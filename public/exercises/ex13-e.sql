SELECT articles.title, users.login, articles.published_at 
	FROM articles
	JOIN users ON articles.user_id = users.id
	ORDER BY articles.published_at DESC
	LIMIT 5;
