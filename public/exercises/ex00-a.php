<?php

$limit = 10;
$indivisibleByTwo = [];

for($i = 0; $i < $limit; $i++) {
	if($i % 2 == 0) {
		$indivisibleByTwo[] = $i;
	}
}

$iterator = 0;
$continue = true;

while($continue) {
	echo $indivisibleByTwo[$iterator];
	unset($indivisibleByTwo[$iterator++]);

	if(!sizeof($indivisibleByTwo)) {
		$continue = false;
	}
}
