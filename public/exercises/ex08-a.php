<?php

class Point {

	protected $x;
	protected $y;

	function __construct(float $x, float $y) {
		$this->x = $x;
		$this->y = $y;
		echo "Point [$this->x, $this->y] has been created.<br>";
	}

	function __destruct() {
		echo "Point [$this->x, $this->y] has been destructed.<br>";
	}

}

new Point(0, 0);
new Point(3, -1);

// // Point [0, 0] has been created.
// // Point [0, 0] has been destructed.
// // Point [3, -1] has been created.
// // Point [3, -1] has been destructed.

$p = new Point(1, 1);
new Point(-4, 0.5);

// Point [1, 1] has been created.
// Point [-4, 0.5] has been created.
// Point [-4, 0.5] has been destructed.
// Point [1, 1] has been destructed.


$p = new Point(3, 3);
unset($p);
new Point(7, -2);

// Point [3, 3] has been created.
// Point [3, 3] has been destructed.
// Point [7, -2] has been created.
// Point [7, -2] has been destructed.
