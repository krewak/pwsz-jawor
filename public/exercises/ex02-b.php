<?php

$scopeVariable = 1;

function doSomething(): void {
	global $scopeVariable;
	echo $scopeVariable;
	$scopeVariable++;
}

doSomething(); // 1
doSomething(); // 2
doSomething(); // 3

echo $scopeVariable; // 4
