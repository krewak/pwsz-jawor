<?php

const DEFAULT_TIME_FORMAT = "Y-m-d";

function itemize(string $item): string {
	return "<li>" . $item . "</li>";
}

$today = date(DEFAULT_TIME_FORMAT);
$day = date(DEFAULT_TIME_FORMAT);
$one_month_later = date(DEFAULT_TIME_FORMAT, strtotime($today . " + 1 week"));

do {
	$day = date(DEFAULT_TIME_FORMAT, strtotime($day . " + 1 day"));
	echo itemize($day);
} while($day != $one_month_later);

// 2017-11-23
// 2017-11-24
// 2017-11-25
// 2017-11-26
// 2017-11-27
// 2017-11-28
// 2017-11-29
