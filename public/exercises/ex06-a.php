<?php

class Point {

	public $x;
	public $y;
	public $name = "S";

	function setX(float $x): void {
		$this->x = $x;
	}

	function setY(float $y): void {
		$this->y = $y;
	}

	function setName(float $name): void {
		$this->name = $name;
	}

	function move(float $x_shift, float $y_shift): void {
		$this->x += $x_shift;
		$this->y += $y_shift;
	}

	function getCoordinates(): string {
		if(isset($this->x) && isset($this->y)) {
			return implode("", [$this->name, ": [", $this->x, ", ", $this->y, "]"]);
		} else {
			return "błąd";
		}
	}

}

$point = new Point;
echo $point->getCoordinates();
echo "<br>";

$point = new Point;
$point->setX(0);
$point->setY(0);
echo $point->getCoordinates();
echo "<br>";

$point = new Point;
$point->setX(0);
$point->setY(0);
$point->move(rand(0, 10), rand(0, 10));
echo $point->getCoordinates();
echo "<br>";

$point = new Point;
$point->setX(0);
$point->setY(0);
$point->move(rand(0, 10), rand(0, 10), "A");
echo $point->getCoordinates();
echo "<br>";
