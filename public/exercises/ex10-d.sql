CREATE TABLE users (
	id int NOT NULL AUTO_INCREMENT,
	login varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	email varchar(255) UNIQUE NOT NULL,
	joined_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id)
);
