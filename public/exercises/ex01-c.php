<?php

// źle:
function fctrl(int $nr): int {
	return $nr < 2 ? 1 : $nr * fctrl($nr - 1);
}

// lepiej:
function factorial(int $number): int {
	if($number < 2) {
		return 1;
	} 

	return $number * factorial($number - 1);
}

echo factorial(1); // 1
echo factorial(2); // 2
echo factorial(3); // 6
echo factorial(4); // 24
echo factorial(5); // 120
