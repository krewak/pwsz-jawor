<?php

function concatenateStrings(string $a, string $b, string $connector = ""): string {
	return $a . $connector . $b;
}

echo concatenateStrings("diffeo", "morphism"); // diffeomorphism
echo concatenateStrings("MS", "DOS", "-"); // MS-DOS
