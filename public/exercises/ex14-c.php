<?php

function itemize(string $string): string {
	return "<li>" . $string . "</li>";
}

$mysqli = mysqli_connect("localhost", "root", "", "jawor");

$query = "SELECT articles.title, users.login, articles.published_at FROM articles JOIN users ON articles.user_id = users.id WHERE YEAR(published_at) = 2017;";

$users = $mysqli->query($query);

if($users->num_rows > 0) {
	foreach($users as $user) {
		echo itemize($user["login"]);
	}
} else {
	echo itemize("brak wyników");
}
