CREATE TABLE users (
	id int,
	login varchar(255),
	password varchar(255),
	email varchar(255),
	joined_at TIMESTAMP
);
