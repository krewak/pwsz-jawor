<?php

$scopeVariable = true;

function doSomething(): void {	
	echo $scopeVariable; // Notice: Undefined variable: scopeVariable in ./ex02-a.php on line 6
}

doSomething();
