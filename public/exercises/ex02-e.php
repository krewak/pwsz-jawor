<?php

function concatenateStrings(string $a, string $b, string $connector): string {
	return $a . $connector . $b;
}

echo concatenateStrings("diffeo", "morphism");

// Fatal error: Uncaught ArgumentCountError:
// Too few arguments to function concatenateStrings(),
// 2 passed in ./ex02-d.php on line 7 and exactly 3 expected in ./ex02-d.php:3
