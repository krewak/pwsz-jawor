<?php

class Point {

	protected $x;
	protected $y;
	protected $name;

	public function __construct(float $x, float $y, string $name = "S") {
		$this->x = $x;
		$this->y = $y;
		$this->name = $name;
	}

	public function getX(): float {
		return $this->x;
	}

	public function getY(): float {
		return $this->y;
	}

	public function getCoordinates(): string {
		if(isset($this->x) && isset($this->y)) {
			return implode("", [$this->name, ": [", $this->x, ", ", $this->y, "]"]);
		} else {
			return "błąd";
		}
	}

}

class Rectangle {

	protected $corners = [];

	public function __construct(Point ...$corners) {
		foreach($corners as $corner) {
			$this->corners[] = $corner;
		}
	}

	public function echoCoordinates(): void {
		foreach($this->corners as $corner) {
			echo $corner->getCoordinates() . "<br>";
		}
	}

}

class Square extends Rectangle {

	public function __construct(Point $center, float $sideLength) {
		$shift = $sideLength / 2;
		$this->corners[] = new Point($center->getX() + $shift, $center->getY() + $shift, "A");
		$this->corners[] = new Point($center->getX() + $shift, $center->getY() - $shift, "B");
		$this->corners[] = new Point($center->getX() - $shift, $center->getY() - $shift, "C");
		$this->corners[] = new Point($center->getX() - $shift, $center->getY() + $shift, "D");
	}

}

$figure = new Rectangle(new Point(2, 2, "A"), new Point(2, -2, "B"), new Point(-2, -2, "C"), new Point(-2, 2, "D"));
$figure->echoCoordinates();

echo "<hr>";

$figure = new Square(new Point(0, 0, "S"), 4);
$figure->echoCoordinates();
