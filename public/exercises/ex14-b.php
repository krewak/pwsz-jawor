<?php

function itemize(string $string): string {
	return "<li>" . $string . "</li>";
}

$mysqli = mysqli_connect("localhost", "root", "", "jawor");

$users = $mysqli->query("SELECT * FROM users ORDER BY login;");

foreach($users as $user) {
	echo itemize($user["login"]);
}
