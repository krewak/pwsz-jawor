<?php

function sum(int $a, int $b): int {
	return $a + $b;
}

echo sum(1, 1); // 2
echo sum(1, 2); // 3
echo sum(1.5, 2); // 3
