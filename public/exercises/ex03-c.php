<?php

function itemize(string $label, string $item): string {
	return "<li><strong>" . $label . "</strong>: " . $item . "</li>";
}

echo itemize("dzień", date("Y-m-d"));
echo itemize("dzień tygodnia", date("N (l)"));
echo itemize("dzień miesiąca", date("j"));
echo itemize("dzień roku", date("z"));
echo itemize("rok przystępny?", date("L") ? "tak" : "nie");
echo itemize("godzina", date("H:i:s"));
echo itemize("czas uniksowy", date("U"));

// dzień: 2017-11-22
// dzień tygodnia: 3 (Wednesday)
// dzień miesiąca: 22
// dzień roku: 325
// rok przystępny?: nie
// godzina: 19:04:00
// czas uniksowy: 1511377440
