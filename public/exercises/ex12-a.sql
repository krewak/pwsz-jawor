INSERT
	INTO users
		(login, password, email)
	VALUES
		("jkowalski", "$2y$10$Rd8CWd8iV9LaLbPWeduknoVbyxyPaoWgANH9uMoIie7mrf28y4hNn", "jan.kowalski@example.com"),
		("pnowak", "$2y$10$uoWf9odWyVMrhe8Ciek8yumA9H2d4IPnoaRnbyPLVNbxNgadWL8i7", "pnowak@example.com"),
		("pzablocki", "$2y$10$dVf9roi8uWLgaHV8yMN8kWeyPN2ydILndbma4ebnhC9A7PRxooWui", "zab@example.com");
