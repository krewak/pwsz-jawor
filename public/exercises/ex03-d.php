<?php

$book_summary = "Po zajęciu Coruscant - serca Nowej Republiki - wydaje się, że nic już nie powstrzyma Yuuzhan Vongów. Nie wszystko jest jednak stracone. Jacen Solo żyje, choć pozostaje jeńcem tajemniczej i okrutnej istoty, która włada Mocą - zupełnie inną niż ta, którą zna młody Jedi. W niewłaściwych rękach ta potężna energia może być śmiertelnie groźna - dla Jacena i dla całej Republiki...";

$summary_array = explode(" ", $book_summary);

echo "Liczba znaków: " . strlen($book_summary);
echo "<br>Liczba słów: " . sizeof($summary_array);
echo "<br>Liczba unikalnych słów: " . sizeof(array_unique($summary_array));

if(isset($_GET["search"])) {
	echo "<br>Czy szukane słowo <strong>" . $_GET["search"] . "</strong> znajduje się w tekście? " . (in_array($_GET["search"], $summary_array) ? "tak" : "nie");
}

// Liczba znaków: 403
// Liczba słów: 61
// Liczba unikalnych słów: 56
// Czy szukane słowo Jedi znajduje się w tekście? tak
