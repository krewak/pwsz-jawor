<?php

function doSomething(): void {
	$functionScopeVariable = true;
}

doSomething();

echo $functionScopeVariable;

// Notice: Undefined variable: scopeVariable in ./ex02-c.php on line 9
