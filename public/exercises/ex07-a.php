<?php

class Classroom {

	protected $students = [];

	function addStudent(Student $student): self {
		$this->students[] = $student;
		return $this;
	}

	function echoStudents(): void {
		foreach($this->students as $student) {
			echo $student->name;
			echo "<br>";
		}
	}

}

class Student {

	public $name;

	private static $first_names = ["Jan", "Adam", "Wojciech", "Marek", "Kamil", "Robert", "Krzysztof", "Janusz", "Jakub"];
	private static $last_names = ["Kowalski", "Nowak", "Kowalczyk", "Lewandowski", "Jankowski", "Mazur", "Wojciechowski", "Kwiatkowski"];

	static function getRandomFirstName(): string {
		return self::$first_names[array_rand(self::$first_names)];
	}

	static function getRandomLastName(): string {
		return self::$last_names[array_rand(self::$last_names)];
	}

}

$classroom = new Classroom;

foreach(range(1, 32) as $i) {
	$student = new Student;
	$student->name = implode(" ", [Student::getRandomFirstName(), Student::getRandomLastName()]);
	$classroom->addStudent($student);
}

$classroom->echoStudents();