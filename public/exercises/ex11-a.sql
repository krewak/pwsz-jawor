CREATE TABLE users (
	id int NOT NULL AUTO_INCREMENT,
	login varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	email varchar(255) UNIQUE NOT NULL,
	joined_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id)
);

CREATE TABLE articles (
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	title varchar(255) NOT NULL,
	content text,
	published_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES users(id)
);
