<?php

function itemize(string $item): string {
	return "<li>" . $item . "</li>";
}

$books = array();

array_push($books, "Zdrajca");
array_push($books, "Metro 2033");
array_push($books, "Hyperion");
array_push($books, "Człowiek z Wysokiego Zamku");
array_push($books, "Wektor pierwszy");

asort($books);

foreach($books as $book) {
	echo itemize($book);
}

// Człowiek z Wysokiego Zamku
// Hyperion
// Metro 2033
// Wektor pierwszy
// Zdrajca
