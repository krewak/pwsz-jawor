<?php-test

function getEntryInfo(): string {
	$words = ["[", date("Y-m-d h:i:s"), "]", "entry from", $_SERVER['REMOTE_ADDR'], "\n"];
	return implode(" ", $words);
}

$directory = "./logs";

if(!file_exists($directory)) {
	mkdir($directory);
}

$filename = $directory . "/" . date("Ymd") . ".log";

file_put_contents($filename, getEntryInfo(), FILE_APPEND);
