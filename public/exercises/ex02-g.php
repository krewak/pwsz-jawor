<?php

function concatenateStrings(string ...$words): string {
	return implode(" ", $words);
}

echo concatenateStrings("Projektowanie", "i", "programowanie", "obiektowe");
// Projektowanie i programowanie obiektowe
