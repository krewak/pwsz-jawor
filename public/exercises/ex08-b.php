<?php

class Point {

	protected $x;
	protected $y;
	protected $name;

	public function __construct(float $x, float $y, string $name = "S") {
		$this->setX($x);
		$this->setY($y);
		$this->setName($name);
	}

	public function setX(float $x): void {
		$this->x = $x;
	}

	public function setY(float $y): void {
		$this->y = $y;
	}

	public function setName(string $name): void {
		$this->name = $name;
	}

	public function move(float $x_shift, float $y_shift): void {
		$this->x += $x_shift;
		$this->y += $y_shift;
	}

	public function getCoordinates(): string {
		if(isset($this->x) && isset($this->y)) {
			return implode("", [$this->name, ": [", $this->x, ", ", $this->y, "]"]);
		} else {
			return "błąd";
		}
	}

}

$point = new Point(0, 0);
echo $point->getCoordinates();
echo "<br>";

$point = new Point(0, 0);
$point->move(rand(0, 10), rand(0, 10));
echo $point->getCoordinates();
echo "<br>";

$point = new Point(0, 0, "A");
$point->move(rand(0, 10), rand(0, 10));
echo $point->getCoordinates();
echo "<br>";
