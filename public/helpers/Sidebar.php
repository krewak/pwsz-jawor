<?php

namespace App;

class Sidebar {

	public static function get(): array {
		return [
			"Podstawy PHP" => [
				"ex01" => "Funkcje",
				"ex02" => "Zmienne i argumenty",
				"ex03" => "Funkcje wbudowane",
				"ex04" => "Operacje na plikach i katalogach",
			],

			"Programowanie obiektowe" => [
				"ex05" => "Obiekty i klasy",
				"ex06" => "Właściwości i metody",
				"ex07" => "Tworzenie obiektów",
				"ex08" => "Konstruktor, destruktor",
				"ex09" => "Hermetyzacja, dziedziczenie",
			],

			"Bazy danych" => [
				"ex10" => "Tworzenie baz danych",
				"ex11" => "Łączenie tabel",
				"ex12" => "Wstawianie do bazy danych",
				"ex13" => "Pobieranie z bazy danych",
				"ex14" => "Bazy danych i PHP",
			],

			"Aplikacje webowe" => [
				"ex15" => "Publikowanie aplikacji",
				"ex16" => "Optymalizacja aplikacji",
				"ex17" => "Testy szybkości aplikacji",
				"ex18" => "SEO",
			],
		];
	}

}
