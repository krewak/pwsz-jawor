<?php

namespace App;

class TemplateName {

	const PATH = "layout/";
	const EXTENSION = ".twig";

	private static $available_exercies = ["ex01", "ex02", "ex03", "ex04", "ex05", "ex06", "ex07", "ex08", "ex09", "ex10", "ex11", "ex12", "ex13", "ex14"];

	public static function get(string $exercise_name): string {
		return self::PATH . self::sanitize($exercise_name) . self::EXTENSION;
	}

	public static function sanitize(string $exercise_name): string {
		if(!$exercise_name) {
			return "index";
		}

		if(in_array($exercise_name, self::$available_exercies)) {
			return $exercise_name;
		}

		return "404";
	}

}
